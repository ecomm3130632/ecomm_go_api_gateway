package main

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"gitlab.com/ecomm3130632/ecomm_go_api_gateway/api"
	"gitlab.com/ecomm3130632/ecomm_go_api_gateway/api/handlers"
	"gitlab.com/ecomm3130632/ecomm_go_api_gateway/config"
	"gitlab.com/ecomm3130632/ecomm_go_api_gateway/pkg/logger"
	"gitlab.com/ecomm3130632/ecomm_go_api_gateway/services"
)

func main() {
	cfg := config.Load()

	fmt.Printf("config: %+v\n", cfg)

	grpcServer, err := services.NewGrpcClients(cfg)
	if err != nil {
		panic(err)
	}
	loggerLevel := logger.LevelDebug
	switch cfg.Environment {
	case config.DebugMode:
		loggerLevel = logger.LevelDebug
	case config.TestMode:
		loggerLevel = logger.LevelDebug
	default:
		loggerLevel = logger.LevelInfo
	}

	log := logger.NewLogger(cfg.ServiceName, loggerLevel)
	defer logger.Cleanup(log)

	r := gin.New()
	r.Use(gin.Logger(), gin.Recovery())

	h := handlers.NewHandler(cfg, log, grpcServer)
	api.SetUpApi(r, &h, cfg)

	fmt.Println("Start api gatewat...")

	err = r.Run(cfg.HTTPPort)
	if err != nil{
		return
	}
}
