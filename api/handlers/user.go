package handlers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/ecomm3130632/ecomm_go_api_gateway/genproto/user_service"
	"gitlab.com/ecomm3130632/ecomm_go_api_gateway/pkg/helper"
)

// CreateUser godoc
// @ID create_user
// @Router /user [POST]
// @Summary Create User
// @Description  Create User
// @Tags User
// @Accept json
// @Produce json
// @Param profile body user_service.CreateUser true "CreateUserRequestBody"
func (h *Handler) CreateUser(c *gin.Context) {
	var user user_service.CreateUser

	err := c.ShouldBindJSON(&user)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.service.UserService().Create(c, &user)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "GRPC error",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusCreated, resp)
}

// GetUserByID godoc
// @ID get_user_by_id
// @Router /user/{id} [GET]
// @Summary Get User  By ID
// @Description Get User  By ID
// @Tags User
// @Accept json
// @Produce json
// @Param id path string true "id"
func (h *Handler) GetUserById(c *gin.Context) {
	userId := c.Param("id")

	if !helper.IsValidUUID(userId) {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC error",
			"message": "Invalid ID",
		})
		return
	}

	resp, err := h.service.UserService().GetById(c, &user_service.UserPrimaryKey{Id: userId})
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "GRPC error",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusAccepted, resp)
}
