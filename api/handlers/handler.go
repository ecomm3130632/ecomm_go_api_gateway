package handlers

import (
	"gitlab.com/ecomm3130632/ecomm_go_api_gateway/config"
	"gitlab.com/ecomm3130632/ecomm_go_api_gateway/pkg/logger"
	"gitlab.com/ecomm3130632/ecomm_go_api_gateway/services"
)

type Handler struct {
	cfg     config.Config
	log     logger.LoggerI
	service services.ServiceManagerI
}

func NewHandler(cfg config.Config, log logger.LoggerI, srvc services.ServiceManagerI) Handler {
	return Handler{
		cfg:     cfg,
		log:     log,
		service: srvc,
	}
}

